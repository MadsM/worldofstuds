using Script;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(WorldGenerator))]
    public class WorldGeneratorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var generator = (WorldGenerator) target;

            if (GUILayout.Button("Generate World"))
            {
                generator.GenerateWorld();
            }

            if (GUILayout.Button("Clear World"))
            {
                generator.ClearWorld();
            }
        }
    }
}
