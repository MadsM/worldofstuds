﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Script
{
    class GreyScaleImageReader : ImageReader<float>
    {
        private ValueSelectionMethod valueSelectionMethod;

        public GreyScaleImageReader(Texture2D texture, int width, ValueSelectionMethod valueSelectionMethod) : base(texture, width)
        {
            this.valueSelectionMethod = valueSelectionMethod;

        }

        protected override float CreateStudPixelValue(int x, int y, int xMinValue, int xMaxValue, int yMinValue, int yMaxValue)
        {
            var values = new List<float>();
            for (var deltaX = xMinValue; deltaX < xMaxValue; deltaX++)
            {
                for (var deltaY = yMinValue; deltaY < yMaxValue; deltaY++)
                {
                    values.Add(texture.GetPixel(deltaX, deltaY).grayscale);
                }
            }

            return valueSelectionMethod switch
            {
                ValueSelectionMethod.Mean => values.Average(),
                ValueSelectionMethod.Median => values.OrderBy(value => value).ToList()[Mathf.FloorToInt(values.Count / 2f)],
                ValueSelectionMethod.Min => values.Min(),
                ValueSelectionMethod.Max => values.Max(),
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}
