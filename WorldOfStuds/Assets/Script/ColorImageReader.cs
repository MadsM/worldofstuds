﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Script
{
    public class ColorImageReader : ImageReader<Material>
    {
        public event Action<int, int, string, Color> CoordinateProcessed;
        public delegate int GetStudHeightDelegate(int x, int y);
        public delegate KeyValuePair<Material, float> GetClosestMaterialDelegate(Color pixelColor, int height);

        private readonly GetClosestMaterialDelegate WeightMaterialByColor;
        private readonly GetStudHeightDelegate GetStudHeight;

        public ColorImageReader(Texture2D texture, int width,
            GetClosestMaterialDelegate getClosestMaterial,
            GetStudHeightDelegate getStudHeight)
            : base(texture, width)
        {
            WeightMaterialByColor = getClosestMaterial;
            GetStudHeight = getStudHeight;
        }

        protected override Material CreateStudPixelValue(int x, int y, int xMinValue, int xMaxValue, int yMinValue, int yMaxValue)
        {
            var values = new Dictionary<Material, float>();
            for (var deltaX = xMinValue; deltaX < xMaxValue; deltaX++)
            {
                for (var deltaY = yMinValue; deltaY < yMaxValue; deltaY++)
                {
                    var color = texture.GetPixel(deltaX, deltaY);
                    var kvp = WeightMaterialByColor(color, GetStudHeight(x, y));

                    if (values.ContainsKey(kvp.Key))
                        values[kvp.Key] = values[kvp.Key] + kvp.Value;
                    else
                        values.Add(kvp.Key, kvp.Value);

                    CoordinateProcessed?.Invoke(x, y, kvp.Key.name, color);
                }
            }



            return values.OrderByDescending(kvp => kvp.Value)
                .First().Key;
        }
    }
}
