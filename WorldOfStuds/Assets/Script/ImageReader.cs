using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;

namespace Script
{
    public abstract class ImageReader<T>
    {
        protected Texture2D texture;
        protected int width;
        protected int height;
        private float aspectRatio;
        private T[,] studResolutionMap;

        public ImageReader(Texture2D texture, int width)
        {
            this.texture = texture;
            this.width = width;

            aspectRatio = (float)texture.width / texture.height;
            height = Mathf.CeilToInt(width / aspectRatio);
        
        }

        protected void TransformToStudSize()
        {
            var pixelsPerStud = Mathf.FloorToInt((float) texture.width / width);
        
            studResolutionMap = new T[width,height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var xMinValue = (x) * pixelsPerStud;
                    var yMinValue = (y) * pixelsPerStud;
                    var xMaxValue = (x + 1) * pixelsPerStud;
                    var yMaxValue = (y + 1) * pixelsPerStud;
                    studResolutionMap[x,y] = CreateStudPixelValue(x, y, xMinValue, xMaxValue, yMinValue, yMaxValue);
                }
            }
        }

        protected abstract T CreateStudPixelValue(int x, int y, int xMinValue, int xMaxValue, int yMinValue, int yMaxValue);

        private Color GetColorAverage(IEnumerable<Color> colors)
        {
            var r = 0f;
            var g = 0f;
            var b = 0f;
            
            foreach (var color in colors)
            {
                r += color.r;
                g += color.g;
                b += color.b;
            }

            r /= colors.Count();
            g /= colors.Count();
            b /= colors.Count();

            return new Color(r, g, b);
        }

        private Color GetMinSaturation(IEnumerable<Color> colors)
        {
            return colors.OrderBy(color =>
            {
                Color.RGBToHSV(color, out var h, out var s, out var v);
                return s;
            }).First();
        }
        
        private Color GetMaxSaturation(IEnumerable<Color> colors)
        {
            return colors.OrderByDescending(color =>
            {
                Color.RGBToHSV(color, out var h, out var s, out var v);
                return s;
            }).First();
        }
        

        public T[,] GetReaderValues()
        {
            TransformToStudSize();
            return studResolutionMap;
        }

    
        public enum ValueSelectionMethod
        {
            Mean,
            Median,
            Min,
            Max
        }
    }
}
