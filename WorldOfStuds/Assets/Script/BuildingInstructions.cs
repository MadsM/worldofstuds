﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Script
{
    public class BuildingInstructions : MonoBehaviour
    {
        [SerializeField] private CameraController cameraController;
        [SerializeField] private WorldGenerator generator;

        [Header("UI Buttons")]
        [SerializeField] private Button nextLayerButton;
        [SerializeField] private Button previousLayerButton;
        [SerializeField] private Button leftButton;
        [SerializeField] private Button rightButton;
        [SerializeField] private Button upButton;
        [SerializeField] private Button downButton;
        [SerializeField] private Button zoomInButton;
        [SerializeField] private Button zoomOutButton;

        private Vector3Int currentPanel = new Vector3Int(-1, 0, 0);
        private bool isZoomedIn = false;

        private void Awake()
        {
            generator = FindObjectOfType<WorldGenerator>();

            leftButton.onClick.AddListener(MoveLeft);
            rightButton.onClick.AddListener(MoveRight);
            upButton.onClick.AddListener(MoveUp);
            downButton.onClick.AddListener(MoveDown);

            nextLayerButton.onClick.AddListener(NextStudLayer);
            previousLayerButton.onClick.AddListener(PreviousStudLayer);

            zoomInButton.onClick.AddListener(ZoomIn);
            zoomInButton.gameObject.SetActive(false);

            zoomOutButton.onClick.AddListener(ZoomOut);
        }

        private void Start()
        {
            NextPanel();
            ZoomOut();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                PreviousStudLayer();
                return;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                NextStudLayer();
                return;
            }

            var checkCurrentPanel = currentPanel;

            if (Input.GetKeyDown(KeyCode.W))
                MoveWithVector(Vector3Int.up);

            else if (Input.GetKeyDown(KeyCode.A))
                MoveWithVector(Vector3Int.left);

            else if (Input.GetKeyDown(KeyCode.S))
                MoveWithVector(Vector3Int.down);

            else if (Input.GetKeyDown(KeyCode.D))
                MoveWithVector(Vector3Int.right);

            if (checkCurrentPanel != currentPanel)
            {
                int x = currentPanel.x % (generator.WidthInPanels);
                int y = currentPanel.y % (generator.WidthInPanels / 2);

                x = x < 0 ? generator.WidthInPanels - 1 : x;
                y = y < 0 ? generator.WidthInPanels / 2 - 1 : y;

                currentPanel = new Vector3Int(x, y, 0);

                FocusOnCurrentPanel();
            }
        }

        void MoveWithVector(Vector3Int moveWithVector)
        {
            var currentPanelStuds = GetStudsInCurrentPanel();

            foreach (var stud in currentPanelStuds)
                stud.ShowStudLayer(int.MaxValue);

            Vector3Int newPanel = currentPanel;
            currentPanel += moveWithVector;

            ValidateNewPanel(newPanel);

            FocusOnCurrentPanel();
        }

        // Validates that the new position is in fact a panel. If not; Set current panel to an actual a panel
        void ValidateNewPanel(Vector3Int checkCurrentPanel)
        {
            int x = currentPanel.x % (generator.WidthInPanels);
            int y = currentPanel.y % (generator.HeightInPanels);

            x = x < 0 ? generator.WidthInPanels - 1 : x;
            y = y < 0 ? generator.HeightInPanels - 1 : y;

            currentPanel = new Vector3Int(x, y, 0);
        }

        private void NextStudLayer()
        {
            currentPanel += Vector3Int.forward;
            FocusOnCurrentPanel();
        }

        private void PreviousStudLayer()
        {
            if (currentPanel.z <= 0)
                return;

            currentPanel += Vector3Int.back;
            FocusOnCurrentPanel();
        }

        private void MoveUp()
        {
            MoveWithVector(Vector3Int.up);
        }

        private void MoveDown()
        {
            MoveWithVector(Vector3Int.down);
        }

        private void MoveLeft()
        {
            MoveWithVector(Vector3Int.left);
        }

        private void MoveRight()
        {
            MoveWithVector(Vector3Int.right);
        }

        private void ZoomIn()
        {
            isZoomedIn = true;

            zoomInButton.gameObject.SetActive(false);
            zoomOutButton.gameObject.SetActive(true);

            nextLayerButton.gameObject.SetActive(true);
            previousLayerButton.gameObject.SetActive(true);

            FocusOnCurrentPanel();
        }

        private void ZoomOut()
        {
            isZoomedIn = false;

            zoomOutButton.gameObject.SetActive(false);
            zoomInButton.gameObject.SetActive(true);

            nextLayerButton.gameObject.SetActive(false);
            previousLayerButton.gameObject.SetActive(false);

            FocusOnCurrentPanel();
            FocusOnEntireWorld();
        }

        private void ChangePanel(string newPanel)
        {
            var coords = newPanel.Split(',');

            if (coords.Length == 2 &&
                int.TryParse(coords[0], out int xCoord) &&
                int.TryParse(coords[1], out int yCoord))
            {
                var maxWidth = generator.WidthInPanels - 1;
                var maxHeight = generator.WidthInPanels / 2 - 1;

                xCoord = xCoord > maxWidth ? maxWidth : xCoord;
                xCoord = xCoord < 0 ? 0 : xCoord;

                yCoord = yCoord > maxHeight ? maxHeight : yCoord;
                yCoord = yCoord < 0 ? 0 : yCoord;

                currentPanel = new Vector3Int(xCoord, yCoord, 0);

                FocusOnCurrentPanel();
                return;
            }
        }

        private void NextPanel()
        {
            if (currentPanel.x >= generator.WidthInPanels - 1)
            {
                if (currentPanel.y >= generator.WidthInPanels / 2 - 1)
                    currentPanel = Vector3Int.zero;
                else
                    currentPanel = new Vector3Int(0, currentPanel.y + 1, 0);
            }
            else
                currentPanel += Vector3Int.right;

            currentPanel.z = 0;
            FocusOnCurrentPanel();
        }

        private void PreviousPanel()
        {
            if (currentPanel.x == 0)
            {
                if (currentPanel.y == 0)
                    currentPanel = new Vector3Int(generator.WidthInPanels - 1, generator.WidthInPanels / 2 - 1, 0);
                else
                    currentPanel = new Vector3Int(generator.WidthInPanels - 1, currentPanel.y - 1, 0);
            }
            else
                currentPanel += Vector3Int.left;

            currentPanel.z = 0;
            FocusOnCurrentPanel();
        }

        private void FocusOnCurrentPanel()
        {
            generator.HideAllStuds();

            var studs = GetStudsInCurrentPanel();

            var maxHeight = studs.Max(stud => stud.MaxHeight);

            if (currentPanel.z > maxHeight)
                currentPanel.z = maxHeight;

            foreach (var stud in studs)
                stud.ShowStudLayer(currentPanel.z);

            if (isZoomedIn)
                cameraController.FocusOnWorldArea(
                    currentPanel.x * WorldGenerator.StudsPerPanel,
                    currentPanel.y * WorldGenerator.StudsPerPanel,
                    WorldGenerator.StudsPerPanel, WorldGenerator.StudsPerPanel, currentPanel.z);
        }

        private IEnumerable<StudModel> GetStudsInCurrentPanel()
        {
            int minX = currentPanel.x * WorldGenerator.StudsPerPanel;
            int maxX = (currentPanel.x + 1) * WorldGenerator.StudsPerPanel;

            int minY = currentPanel.y * WorldGenerator.StudsPerPanel;
            int maxY = (currentPanel.y + 1) * WorldGenerator.StudsPerPanel;

            var studs = generator.GetStudsInQuadrant(new Vector2Int(minX, minY), new Vector2Int(maxX, maxY));
            return studs;
        }

        private void FocusOnEntireWorld()
        {
            cameraController.FocusOnEntireWorld(
                generator.WidthInPanels * WorldGenerator.StudsPerPanel,
                generator.HeightInPanels * WorldGenerator.StudsPerPanel);
        }

    }
}
