using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StudModel : MonoBehaviour
{
    [Header("Lego Models")]
    [SerializeField] private GameObject legoTile;
    [SerializeField] private GameObject legoFlatTile;

    [SerializeField] private float studDeltaHeight;

    [SerializeField] private Material heightMaterial;

    public int MaxHeight => legoTiles.Length - 1;

    private GameObject[] legoTiles;
    private float[] layerTransparency;

    private void Awake()
    {
        legoTiles = GetComponentsInChildren<MeshRenderer>()
            .Select(t => t.gameObject)
            .ToArray();

        layerTransparency = Enumerable.Repeat(1f, legoTiles.Length)
            .ToArray();
    }

    public void Initialize(int studHeight, Material material)
    {
        var localPosition = Vector3.zero;

        legoTiles = new GameObject[studHeight + 1];
        layerTransparency = new float[studHeight + 1];

        for (int i = 0; i < studHeight - 1; i++)
        {
            var heightTile = Instantiate(legoTile, transform.position + localPosition, transform.rotation, transform);
            heightTile.GetComponentInChildren<MeshRenderer>().material = heightMaterial;

            localPosition += Vector3.forward * studDeltaHeight;
            legoTiles[i] = heightTile;
            layerTransparency[i] = 1; // Starts visible
        }

        var colorFlatTile = Instantiate(legoFlatTile, transform.position + localPosition, transform.rotation, transform);
        colorFlatTile.GetComponentInChildren<MeshRenderer>().material = material;
        legoTiles[studHeight] = colorFlatTile;
    }

    public void HideStud()
    {
        var lowTransparence = .2f;

        for (int i = 0; i < legoTiles.Length; i++)
        {
            if (layerTransparency[i] != lowTransparence)
            {
                SetTransparenceOnStud(legoTiles[i], lowTransparence);
                layerTransparency[i] = lowTransparence;
            }
        }
    }

    public void ShowStudLayer(int levelHeight)
    {
        var lowTransparence = .2f;
        var fullVisibility = 1f;

        for (int i = 0; i < legoTiles.Length; i++)
        {
            if (levelHeight < i)
            {
                legoTiles[i].SetActive(false);
                continue;
            }
            else
                legoTiles[i].SetActive(true);

            var transparencyLevel = i == levelHeight ? fullVisibility : lowTransparence;

            if (layerTransparency[i] != transparencyLevel)
            {
                SetTransparenceOnStud(legoTiles[i], transparencyLevel);
                layerTransparency[i] = transparencyLevel;
            }
        }
    }

    private void SetTransparenceOnStud(GameObject studStack, float transparence)
    {
        var renderer = studStack.GetComponentInChildren<Renderer>();

        var color = renderer.material.color;
        renderer.material.color = new Color(color.r, color.g, color.b, transparence);
    }
}
