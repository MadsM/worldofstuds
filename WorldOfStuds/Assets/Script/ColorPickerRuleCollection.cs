using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Script;
using System;
using UnityEditor;

[CreateAssetMenu(fileName = "Rules", menuName = "ScriptableObjects/ColorRule", order = 1)]
public class ColorPickerRuleCollection : ScriptableObject
{
    [SerializeField] private Material material;
    [SerializeField] private float weight = 1;
    [SerializeField] private List<ColorPickerRule> rules;

    public Material Material => material;

    public float GetMatchingColorWeight(Color color, int height)
    {
        var systemColor = SystemColorUtility.ToDrawingColor(color);

        foreach (var rule in rules)
        {
            if (rule.MatchRule(systemColor, height) == false)
                return -1;
        }

        return weight;
    }

    [Serializable]
    private class ColorPickerRule
    {
        [SerializeField] private ColorPropertyType firstColorProperty;
        [SerializeField] private OperatorType matchOperator;
        [SerializeField] private ColorPropertyType secondColorProperty = ColorPropertyType.NotUsed;
        [SerializeField] private int value;

        public bool MatchRule(System.Drawing.Color color, int height)
        {
            if (secondColorProperty != ColorPropertyType.NotUsed && value != 0)
                throw new ArgumentException("Cannot use both 'NotUsed' and 'value' at the same time");

            switch (firstColorProperty)
            {
                case ColorPropertyType.Brightness: return MatchOperator(SystemColorUtility.getBrightness(color) * 100);
                case ColorPropertyType.Saturation:
                    {
                        var saturation = color.GetSaturation() * 100;
                        return MatchOperator(saturation);
                    }
                case ColorPropertyType.Red:
                case ColorPropertyType.Green:
                case ColorPropertyType.Blue:
                    {
                        var firstColor = GetRGBValue(firstColorProperty, color);
                        var secondColor = secondColorProperty == ColorPropertyType.NotUsed ? value : GetRGBValue(secondColorProperty, color);
                        return MatchOperator(firstColor, secondColor);
                    }
                case ColorPropertyType.Height:
                    {
                        return MatchOperator(height);
                    }
            }

            return false;
        }

        private int GetRGBValue(ColorPropertyType colorProperty, System.Drawing.Color color)
        {
            switch (colorProperty)
            {
                case ColorPropertyType.Red: return color.R;
                case ColorPropertyType.Green: return color.G;
                case ColorPropertyType.Blue: return color.B;
                default:
                    throw new NotImplementedException();
            }
        }

        private bool MatchOperator(float valueToMatch)
        {
            return MatchOperator(valueToMatch, value);
        }

        private bool MatchOperator(float value1, float value2)
        {
            switch (matchOperator)
            {
                case OperatorType.BiggerThan: return value1 > value2;
                case OperatorType.LowerThan: return value1 < value2;
                default:
                    throw new NotImplementedException();
            }
        }


        private enum ColorPropertyType
        {
            Red,
            Green,
            Blue,
            Brightness,
            Saturation,
            NotUsed,
            Height
        }

        private enum OperatorType
        {
            BiggerThan,
            LowerThan
        }
    }

}
