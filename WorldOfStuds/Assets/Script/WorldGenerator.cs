using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Script
{
    public class WorldGenerator : MonoBehaviour
    {
        [SerializeField] private CameraController cameraController;

        [Header("World Maps")]
        [SerializeField] private Texture2D oceanMap;
        [SerializeField] private Texture2D toplogyMap;
        [SerializeField] private Texture2D bathymetryMap;
        [SerializeField] private Texture2D VegetationMap;

        [SerializeField] private StudModel StudPrefab;

        [Header("Height")]
        [SerializeField] private AnimationCurve heightThreshold;

        [Header("Materials")]
        [SerializeField] private AnimationCurve waterDepthThreshold;
        [SerializeField] private List<Material> waterMaterials;
        [SerializeField] private Material missingColorRuleMaterial;

        [Header("Color Rules")]
        [SerializeField] private List<ColorPickerRuleCollection> colorRules;

        [Header("Logging")]
        [SerializeField] private bool logColorUsage;
        [SerializeField] private List<Vector2Int> coordinatesToReport;

        [SerializeField] public int WidthInPanels;
        [SerializeField] public int HeightInPanels;

        [SerializeField] private GreyScaleImageReader.ValueSelectionMethod landWaterSelectionMethod;

        public const int StudsPerPanel = 16;

        private float[,] isWaterMap;
        private float[,] heightMap;
        private float[,] depthMap;
        private Material[,] colorMap;

        private Dictionary<Vector2Int, StudModel> worldOfStuds = new Dictionary<Vector2Int, StudModel>();

        public float[,] WorldCoordinates => isWaterMap;
        public int NumberOfPanels => WidthInPanels * HeightInPanels;

        private void Awake()
        {
            var studs = GetComponentsInChildren<StudModel>();
            foreach (var stud in studs)
            {
                worldOfStuds.Add(new Vector2Int((int)stud.transform.position.x, (int)stud.transform.position.y), stud);
            }
        }

        public void GenerateWorld()
        {
            ClearWorld();
            SetConfigurations();

            var mapWidth = isWaterMap.GetLength(0);
            var mapHeight = isWaterMap.GetLength(1);

            var legoTilesUsed = 0;
            var coloredFlatTilesUsed = new Dictionary<Material, int>();

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    var coordinate = new Vector3(x, y, 0);
                    var targetPosition = this.transform.position + coordinate;

                    Material material;

                    if (isWaterMap[x, y] > 0)
                        material = GetWaterMaterial(depthMap[x, y]);
                    else
                        material = colorMap[x, y];

                    if (coloredFlatTilesUsed.ContainsKey(material))
                        coloredFlatTilesUsed[material]++;
                    else
                        coloredFlatTilesUsed.Add(material, 1);


                    var studModel = Instantiate(StudPrefab, targetPosition, StudPrefab.transform.rotation, transform);
                    int studHeight = GetNumberOfStudHeight(x, y);
                    studModel.Initialize(studHeight, material);
                    studModel.name = $"{coordinate.x}, {coordinate.y}";
                    worldOfStuds.Add(new Vector2Int(x, y), studModel);

                    if (studHeight > 0)
                        legoTilesUsed += studHeight - 1;
                }
            }

            cameraController.OnWorldMapUpdated(WidthInPanels * StudsPerPanel, HeightInPanels * StudsPerPanel);

            if (logColorUsage)
            {
                foreach (var kvp in coloredFlatTilesUsed)
                {
                    Debug.Log($"{kvp.Key.name} flat tiles used: {kvp.Value}\n");
                }

                Debug.Log($"White tiles with stud used: {legoTilesUsed}\n");
                Debug.Log($"Total number of colored flat tiles used: {coloredFlatTilesUsed.Sum(kvp => kvp.Value)}\n");
            }
        }

        public IEnumerable<StudModel> GetStudsInQuadrant(Vector2Int studsFrom, Vector2Int studsTo)
        {
            for (int x = studsFrom.x; x < studsTo.x; x++)
            {
                for (int y = studsFrom.y; y < studsTo.y; y++)
                {
                    var coords = new Vector2Int(x, y);
                    yield return worldOfStuds[coords];
                }
            }
        }

        public void ClearWorld()
        {
            foreach (var kvp in worldOfStuds)
                DestroyImmediate(kvp.Value.gameObject);

            worldOfStuds.Clear();

            for (int i = this.transform.childCount; i > 0; --i)
                DestroyImmediate(this.transform.GetChild(0).gameObject);
        }

        public void HideAllStuds()
        {
            foreach (var kvp in worldOfStuds)
                kvp.Value.HideStud();
        }

        private void SetConfigurations()
        {
            var mapWidthInStuds = WidthInPanels * StudsPerPanel;

            isWaterMap = new GreyScaleImageReader(oceanMap, mapWidthInStuds, landWaterSelectionMethod).GetReaderValues();
            heightMap = new GreyScaleImageReader(toplogyMap, mapWidthInStuds, GreyScaleImageReader.ValueSelectionMethod.Max).GetReaderValues();
            depthMap = new GreyScaleImageReader(bathymetryMap, mapWidthInStuds, GreyScaleImageReader.ValueSelectionMethod.Min).GetReaderValues();

            var colorReader = new ColorImageReader(VegetationMap, mapWidthInStuds, WeightMaterialByColor, GetNumberOfStudHeight);
            colorReader.CoordinateProcessed += RapportCoordinateProcessed;
            colorMap = colorReader.GetReaderValues();
        }

        private void RapportCoordinateProcessed(int x, int y, string materialName, Color color)
        {
            var coordinate = new Vector2Int(x, y);
            if (coordinatesToReport.Contains(coordinate))
            {
                var drawingColor = SystemColorUtility.ToDrawingColor(color);

                Debug.Log($"Rapport: {materialName}{Environment.NewLine}" +
                    $" {x}, {y}{Environment.NewLine}" +
                    $"R: {color.r}{Environment.NewLine}" +
                    $"G: {color.g}{Environment.NewLine}" +
                    $"B: {color.b}{Environment.NewLine}" +
                    $"Brightness: {SystemColorUtility.getBrightness(drawingColor) * 100}{Environment.NewLine}" +
                    $"Saturation: {drawingColor.GetSaturation() * 100}");
            }
        }

        private int GetNumberOfStudHeight(int x, int y)
        {
            if (isWaterMap[x, y] > 0)
                return 0;

            return Mathf.RoundToInt(heightThreshold.Evaluate(heightMap[x, y]));
        }

        private Material GetWaterMaterial(float value)
        {
            var evaluation = waterDepthThreshold.Evaluate(value);
            var materialIndex = Mathf.RoundToInt(evaluation * waterMaterials.Count);

            if (materialIndex == 0)
                materialIndex++;

            return waterMaterials[--materialIndex];
        }

        private KeyValuePair<Material, float> WeightMaterialByColor(Color color, int height)
        {
            foreach (var rule in colorRules)
            {
                var matchingValue = rule.GetMatchingColorWeight(color, height);

                if (matchingValue != -1)
                    return new KeyValuePair<Material, float>(rule.Material, matchingValue);
            }

            return new KeyValuePair<Material, float>(missingColorRuleMaterial, -1);
        }

        private float ColorDistance(Color colorA, Color colorB)
        {
            var vectorA = new Vector3(colorA.r, colorA.g, colorA.b);
            var vectorB = new Vector3(colorB.r, colorB.g, colorB.b);
            return Vector3.Distance(vectorA, vectorB);
        }

        private Color NormalizeSaturation(Color colorToNormalize, Color referenceColor)
        {
            UnityEngine.Color.RGBToHSV(colorToNormalize, out var h, out var s, out var v);
            Color.RGBToHSV(referenceColor, out var _, out s, out v);

            return Color.HSVToRGB(h, s, v);
        }
    }
}
