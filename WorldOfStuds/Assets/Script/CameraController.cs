using UnityEngine;

namespace Script
{
    [ExecuteAlways]
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float heightPerLayer;
        [SerializeField] private float cameraZoomZoomedOut = 40;
        
        private Camera mainCamera;

        private void Start()
        {
            mainCamera = GetComponent<Camera>();
        }
        
        public void OnWorldMapUpdated(int width, int height)
        {
            mainCamera.transform.rotation = new Quaternion(0,0,0,0);
            var necessaryHeight = Mathf.Max(width / mainCamera.aspect, height);
            mainCamera.orthographicSize = necessaryHeight / 2f;
            mainCamera.transform.position = new Vector3(width / 2f - .5f, heightPerLayer + height / 2f - .5f, -10);
        }

        public void FocusOnWorldArea(int xPos, int yPos, int width, int height, int layerHeight)
        {
            mainCamera.transform.rotation = Quaternion.Euler(-30,0,0);
            var necessaryHeight = Mathf.Max(width / mainCamera.aspect, height);
            mainCamera.orthographicSize = necessaryHeight / 2f;
            mainCamera.transform.position = new Vector3(xPos + width / 2f - .5f, yPos + (layerHeight * heightPerLayer) - 5.5f + height / 2f - .5f, -10);
        }

        public void FocusOnEntireWorld(int worldWidth, int worldHeight)
        {
            mainCamera.orthographicSize = cameraZoomZoomedOut;
            mainCamera.transform.position = new Vector3(worldWidth / 2, worldHeight / 2, -10);
            mainCamera.transform.rotation = new Quaternion(0,0,0,0);
        }
    }
}
